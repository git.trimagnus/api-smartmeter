<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Node;
use Illuminate\Support\Facades\Validator;

class NodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Node::query();
        $query = $query->whereCompany_id($request->company_id);

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $nodes = $query->get();
        $nodes->load(['devicetype', 'servicetype']);

        $response = [
            'status' => 'success',
            'data' => $nodes
        ];
        return response()->json($response, 200);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'gateway_id' => 'required|exists:gateways,id',
            'devicetype_id' => 'required|exists:devicetypes,id',
            'servicetype_id' => 'required|exists:servicetypes,id',
            'date_installation' => 'required|date_format:Y-m-d H:i:s',
            'device_eui' => 'required|string|unique:nodes,device_eui',
            'mac_address' => 'required|string',
            'meter_number' => 'present|nullable|string',
            'cronjob' => 'present|nullable|string',
            'image_node' => 'present|nullable|string',
            'battery' => 'present|nullable|string',
            'valve' => 'present|nullable|string',
            'rssi' => 'present|nullable|string',
            'noise' => 'present|nullable|string',
            'last_usage' => 'present|nullable|string',
            'last_balance' => 'present|nullable|string',
            'last_update' => 'present|nullable|date_format:Y-m-d H:i:s',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_node)) {
            $image_node = substr($request->image_node, strpos($request->image_node, ",")+1);
            $image_node_name = time() . '.' . str_random(20) . '.jpg'; 
            $image_node_path = public_path() . "/node/" . $image_node_name;
            file_put_contents($image_node_path, base64_decode($image_node));
            $image_node_url = env('API_URL'). "/node/" . $image_node_name;
        } else {
            $image_node_url = env('API_URL'). "/other/no_image.png";
        }

        $node = Node::create([
            'company_id' => $request->company_id,
            'gateway_id' => $request->gateway_id,
            'devicetype_id' => $request->devicetype_id,
            'servicetype_id' => $request->servicetype_id,
            'date_installation' => $request->date_installation,
            'device_eui' => $request->device_eui,
            'mac_address' => $request->mac_address,
            'meter_number' => $request->meter_number,
            'cronjob' => $request->cronjob,
            'image_node' => $image_node_url,
            'battery' => $request->battery,
            'valve' => $request->valve,
            'rssi' => $request->rssi,
            'noise' => $request->noise,
            'last_usage' => $request->last_usage,
            'last_balance' => $request->last_balance,
            'last_update' => $request->last_update,
            'status' => $request->status,
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $node
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Node::query();
        $query = $query->whereCompany_id($request->company_id);

        $node = $query->findOrFail($id);
        $node->load(['gateway', 'devicetype', 'servicetype']);

        $response = [
            'status' => 'success',
            'data' => $node
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'gateway_id' => 'required|exists:gateways,id',
            'devicetype_id' => 'required|exists:devicetypes,id',
            'servicetype_id' => 'required|exists:servicetypes,id',
            'date_installation' => 'required|date_format:Y-m-d H:i:s',
            // 'device_eui' => 'required|string|unique:nodes,device_eui',
            'mac_address' => 'required|string',
            'meter_number' => 'present|nullable|string',
            'cronjob' => 'present|nullable|string',
            'image_node' => 'present|nullable|string',
            'battery' => 'present|nullable|string',
            'valve' => 'present|nullable|string',
            'rssi' => 'present|nullable|string',
            'noise' => 'present|nullable|string',
            'last_usage' => 'present|nullable|string',
            'last_balance' => 'present|nullable|string',
            'last_update' => 'present|nullable|date_format:Y-m-d H:i:s',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $node = Node::findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_node)) {
            $image_node = substr($request->image_node, strpos($request->image_node, ",")+1);
            $image_node_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_node_path = public_path() . "/node/" . $image_node_name;
            file_put_contents($image_node_path, base64_decode($image_node));
            $image_node_url = env('API_URL'). "/node/" . $image_node_name;
            $node->update([
                'image_node' => $image_node_url,
            ]);
        }   
        
        $node->update([
            'company_id' => $request->company_id,
            'gateway_id' => $request->gateway_id,
            'devicetype_id' => $request->devicetype_id,
            'servicetype_id' => $request->servicetype_id,
            'date_installation' => $request->date_installation,
            // 'device_eui' => $request->device_eui,
            'mac_address' => $request->mac_address,
            'meter_number' => $request->meter_number,
            'cronjob' => $request->cronjob,
            // 'image_node' => $image_node_url,
            'battery' => $request->battery,
            'valve' => $request->valve,
            'rssi' => $request->rssi,
            'noise' => $request->noise,
            'last_usage' => $request->last_usage,
            'last_balance' => $request->last_balance,
            'last_update' => $request->last_update,
            'status' => $request->status,
        ]);
        
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' =>  $node
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $node = Node::whereCompany_id($request->company_id)->findOrFail($id);
        $node->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}