<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Powersource;
use Illuminate\Support\Facades\Validator;

class PowersourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Powersource::query();

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $powersources = $query->get();

        $response = [
            'status' => 'success',
            'data' => $powersources
        ];
        return response()->json($response, 200);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $powersource = Powersource::create($request->all());

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $powersource
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $powersource = Powersource::findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $powersource
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $powersource = Powersource::findOrFail($id);
        $powersource->update($request->all());
        
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' =>  $powersource
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $powersource = Powersource::findOrFail($id);
        $powersource->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}