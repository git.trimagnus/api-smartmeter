<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;
use App\Repositories\Sindcon\Api\NodeRepository;

class SyncApiSindconNodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-api-sindcon-nodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync API Sindcon Nodes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = Company::with('nodes')->whereMethod('api')->whereNetworkserver_id(1)->get();
        
        foreach ($companies as $company) {
            foreach ($company->nodes as $node) {
                $nodeRepository = new NodeRepository();
                $nodeRepository->storeAndUpdateNode($company->url_middleware, $node->device_eui);
            }
        }
    }
}
