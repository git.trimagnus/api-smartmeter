<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servicetype extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'unit'];

    public function node()
    {
      return $this->hasOne(Node::class);
    }

    public function voucher()
    {
      return $this->hasOne(Voucher::class);
    }

    public function pricelist()
    {
      return $this->hasOne(Pricelist::class);
    }
}