<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pricelist extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'servicetype_id', 'name', 'type', 'description' ,'price',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function servicetype()
    {
        return $this->belongsTo(Servicetype::class);
    }
}