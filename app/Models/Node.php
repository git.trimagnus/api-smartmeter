<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Node extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'gateway_id', 'devicetype_id', 'servicetype_id', 
        'date_installation', 'device_eui', 'mac_address', 'meter_number',          
        'cronjob', 'image_node', 'battery', 'valve', 'rssi',
        'noise', 'last_usage', 'last_balance', 'last_update', 'status',    
    ];  

    public function company()
    {
      return $this->belongsTo(Company::class);
    }

    public function gateway()
    {
      return $this->belongsTo(Gateway::class);
    }

    public function devicetype()
    {
      return $this->belongsTo(Devicetype::class);
    }

    public function servicetype()
    {
      return $this->belongsTo(Servicetype::class);
    }

    public function nodehistories()
    {
      return $this->hasMany(Nodehistory::class);
    }
}
