<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gateway extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'unitmodel_id', 'powersource_id', 'date_installation',           
        'gateway_name', 'mac', 'description', 'sim1_provider', 'sim1_number',          
        'sim2_provider', 'sim2_name', 'image_gateway', 'last_update', 'status',    
    ];  

    public function company()
    {
      return $this->belongsTo(Company::class);
    }

    public function unitmodel()
    {
      return $this->belongsTo(Unitmodel::class);
    }

    public function powersource()
    {
      return $this->belongsTo(Powersource::class);
    }

    public function nodes()
    {
      return $this->hasMany(Node::class);
    }

}