<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nodehistory extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'node_id', 'device_eui', 'totalizer', 
        'usage', 'balance', 'valve', 'battery',          
        'rssi', 'noise', 'last_update',   
    ];  

    public function company()
    {
      return $this->belongsTo(Company::class);
    }

    public function node()
    {
      return $this->belongsTo(Node::class);
    }

}
