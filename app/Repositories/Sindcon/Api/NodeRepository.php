<?php

namespace App\Repositories\Sindcon\Api;

use App\Models\Node;
use App\Models\Nodehistory;
use Carbon\Carbon;

class NodeRepository {

    public function findByDeviceEUI($middlewareApiHost, $deviceEUI)
    {
        $client = new \GuzzleHttp\Client();

        $res = $client->get($middlewareApiHost.'/api/v1/nodes/'.$deviceEUI);
        // $statusCode = $res->getStatusCode();
        // $response = json_decode($res->getBody());
        $result = $res->getBody();
        return $result;
      
    }

    public function storeAndUpdateNode($middlewareApiHost, $deviceEUI)
    {
        // $node = json_decode($this->findByDeviceEUI($deviceEUI));
        $node = Node::whereDevice_eui($deviceEUI)->firstOrFail();
        $nodeResult = json_decode($this->findByDeviceEUI($middlewareApiHost, $deviceEUI));
        $nodeHistory = $node->nodehistories()->latest()->first();
        $lastUpdateHistory = ($nodeHistory) ? $nodeHistory->last_update : null;

        if ($nodeResult->data->lastUpdate != $lastUpdateHistory) {
            $lastTotalizer = ($nodeHistory) ? $nodeHistory->totalizer : $nodeResult->data->meterReading;
            $usage = number_format($nodeResult->data->meterReading - $lastTotalizer, 3);

            $node->update([
                'battery' => $nodeResult->data->battery,
                'valve' => $nodeResult->data->valve,
                'rssi' => $nodeResult->data->rssi,
                'noise' => null,
                'last_usage' => $usage,
                'last_balance' => null,
                'last_update' => $nodeResult->data->lastUpdate,
            ]);

            $nodehistory = Nodehistory::create([
                'company_id' => $node->company_id,
                'node_id' => $node->id,
                'device_eui' => $node->device_eui,
                'totalizer' => $nodeResult->data->meterReading,
                'usage' => $usage,
                'balance' => null,
                'valve' => $nodeResult->data->valve,
                'battery' => $nodeResult->data->battery,
                'rssi' => $nodeResult->data->rssi,
                'noise' => null,
                'last_update' => $nodeResult->data->lastUpdate
            ]);
        }
        

    }
}