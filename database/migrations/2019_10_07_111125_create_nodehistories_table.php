<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodehistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodehistories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('node_id');
            $table->foreign('node_id')->references('id')->on('nodes')->onDelete('cascade');
            $table->string('device_eui');
            $table->string('totalizer')->nullable();
            $table->string('usage')->nullable();
            $table->string('balance')->nullable();
            $table->string('valve')->nullable();
            $table->string('battery')->nullable();
            $table->string('rssi')->nullable();
            $table->string('noise')->nullable();
            $table->dateTime('last_update')->nullable();  
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodehistories');
    }
}
