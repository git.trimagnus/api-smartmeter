<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateways', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('unitmodel_id');
            $table->foreign('unitmodel_id')->references('id')->on('unitmodels')->onDelete('cascade');
            $table->unsignedBigInteger('powersource_id');
            $table->foreign('powersource_id')->references('id')->on('powersources')->onDelete('cascade');
            $table->dateTime('date_installation');
            $table->string('gateway_name');
            $table->string('mac')->unique();
            $table->longText('description')->nullable();
            $table->string('sim1_provider');
            $table->string('sim1_number');
            $table->string('sim2_provider')->nullable();
            $table->string('sim2_name')->nullable();
            $table->string('image_gateway')->nullable();
            $table->dateTime('last_update')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gateways');
    }
}
