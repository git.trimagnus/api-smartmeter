<?php

use Illuminate\Database\Seeder;

class PowersourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('powersources')->insert([
            'id' => 1,
            'name' => 'DC Cable',
        ]);

        DB::table('powersources')->insert([
            'id' => 2,
            'name' => 'ACCU Battery',
        ]);

        DB::table('powersources')->insert([
            'id' => 3,
            'name' => 'Solar Panel',
        ]);
    }
}
