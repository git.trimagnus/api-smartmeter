<?php

use Illuminate\Database\Seeder;

class NetworkserversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('networkservers')->insert([
            'id' => 1,
            'name' => 'Sindcon',
        ]);
    }
}
