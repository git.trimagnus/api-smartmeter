<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'id' => 1,
            'company_id' => 1,
            'customer_name' => 'PT Demo Customer',
            'customer_phone' => 'xxxxx',
            'customer_email' => 'info@demo.co.id',
            'customer_address' => 'xxxxx',
            'customer_website' => 'xxxx',
            'image_customer' => '',
            'pic_name' => 'demo',
            'pic_phone' => 'xxxxx',
            'pic_email' => 'info@demo.co.id',
            'status' => 1,
        ]);
    }
}
