<?php

use Illuminate\Database\Seeder;

class DevicetypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('devicetypes')->insert([
            'id' => 1,
            'name' => 'Reed Switch',
        ]);

        DB::table('devicetypes')->insert([
            'id' => 2,
            'name' => 'Postpaid Meter',
        ]);

        DB::table('devicetypes')->insert([
            'id' => 3,
            'name' => 'Prepaid Meter',
        ]);
    }
}
