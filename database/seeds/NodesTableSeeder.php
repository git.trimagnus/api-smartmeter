<?php

use Illuminate\Database\Seeder;

class NodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return voids
     */
    public function run()
    {
        DB::table('nodes')->insert([
            'id' => 1,
            'company_id' => 1,
            'gateway_id' => 1,
            'devicetype_id' => 1,
            'servicetype_id' => 1,
            'date_installation' => '2018-08-04 20:32:59',
            'device_eui' => '0800000010000029',
            'mac_address' => '0800000010000029',
            'meter_number' => '',
            'cronjob' => '',
            'image_node' => 'base64encode',
            'battery' => '',
            'valve' => '',
            'rssi' => '',
            'noise' => '',
            'last_usage' => '',
            'last_balance' => '',
            'last_update' => null,
            'status' => 1,
        ]);
    }
}
