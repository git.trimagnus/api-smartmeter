<?php

use Illuminate\Database\Seeder;

class GatewaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return voids
     */
    public function run()
    {
        DB::table('gateways')->insert([
            'id' => 1,
            'company_id' => 1,
            'unitmodel_id' => 1,
            'powersource_id' => 1,
            'date_installation' => '2018-08-04 20:32:59',
            'gateway_name' => 'cobaa',
            'mac' => 'asdsdd',
            'description' => 'optional',
            'sim1_provider' => 'string required',
            'sim1_number' => 'string required',
            'sim2_provider' => 'optional',
            'sim2_name' => 'optional',
            'image_gateway' => 'base64encode',
            'last_update' => '2019-09-26 16:58:14',
            'status' => 1,
        ]);
    }
}
