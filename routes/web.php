<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();  
});

$router->get('coba', 'ExampleController@coba');

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->post('/login', 'V1\AuthController@login');

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('me', 'V1\AuthController@me');

        $router->get('networkservers', 'V1\NetworkserverController@index');
        $router->post('networkservers', 'V1\NetworkserverController@store');
        $router->get('networkservers/{id}', 'V1\NetworkserverController@show');
        $router->put('networkservers/{id}', 'V1\NetworkserverController@update');
        $router->delete('networkservers/{id}', 'V1\NetworkserverController@destroy');

        $router->get('companies', 'V1\CompanyController@index');
        $router->post('companies', 'V1\CompanyController@store');
        $router->get('companies/{id}', 'V1\CompanyController@show');
        $router->put('companies/{id}', 'V1\CompanyController@update');
        $router->delete('companies/{id}', 'V1\CompanyController@destroy');

        $router->get('customers', 'V1\CustomerController@index');
        $router->post('customers', 'V1\CustomerController@store');
        $router->get('customers/{id}', 'V1\CustomerController@show');
        $router->put('customers/{id}', 'V1\CustomerController@update');
        $router->delete('customers/{id}', 'V1\CustomerController@destroy');

        $router->get('users', 'V1\UserController@index');
        $router->post('users', 'V1\UserController@store');
        $router->get('users/{id}', 'V1\UserController@show');
        $router->put('users/{id}', 'V1\UserController@update');
        $router->delete('users/{id}', 'V1\UserController@destroy');

        $router->get('roles', 'V1\RoleController@index');
        $router->post('roles', 'V1\RoleController@store');
        $router->get('roles/{id}', 'V1\RoleController@show');
        $router->put('roles/{id}', 'V1\RoleController@update');
        $router->delete('roles/{id}', 'V1\RoleController@destroy');

        $router->get('servicetypes', 'V1\ServicetypeController@index');
        $router->post('servicetypes', 'V1\ServicetypeController@store');
        $router->get('servicetypes/{id}', 'V1\ServicetypeController@show');
        $router->put('servicetypes/{id}', 'V1\ServicetypeController@update');
        $router->delete('servicetypes/{id}', 'V1\ServicetypeController@destroy');

        $router->get('devicetypes', 'V1\DevicetypeController@index');
        $router->post('devicetypes', 'V1\DevicetypeController@store');
        $router->get('devicetypes/{id}', 'V1\DevicetypeController@show');
        $router->put('devicetypes/{id}', 'V1\DevicetypeController@update');
        $router->delete('devicetypes/{id}', 'V1\DevicetypeController@destroy');

        $router->get('unitmodels', 'V1\UnitmodelController@index');
        $router->post('unitmodels', 'V1\UnitmodelController@store');
        $router->get('unitmodels/{id}', 'V1\UnitmodelController@show');
        $router->put('unitmodels/{id}', 'V1\UnitmodelController@update');
        $router->delete('unitmodels/{id}', 'V1\UnitmodelController@destroy');

        $router->get('pricelists', 'V1\PricelistController@index');
        $router->post('pricelists', 'V1\PricelistController@store');
        $router->get('pricelists/{id}', 'V1\PricelistController@show');
        $router->put('pricelists/{id}', 'V1\PricelistController@update');
        $router->delete('pricelists/{id}', 'V1\PricelistController@destroy');

        $router->get('vouchers', 'V1\VoucherController@index');
        $router->post('vouchers', 'V1\VoucherController@store');        
        $router->get('vouchers/{id}', 'V1\VoucherController@show');
        $router->put('vouchers/{id}', 'V1\VoucherController@update');
        $router->delete('vouchers/{id}', 'V1\VoucherController@destroy');

        $router->get('gateways', 'V1\GatewayController@index');
        $router->post('gateways', 'V1\GatewayController@store');
        $router->get('gateways/{id}', 'V1\GatewayController@show');
        $router->put('gateways/{id}', 'V1\GatewayController@update');
        $router->delete('gateways/{id}', 'V1\GatewayController@destroy');

        $router->get('nodes', 'V1\NodeController@index');
        $router->post('nodes', 'V1\NodeController@store');
        $router->get('nodes/{id}', 'V1\NodeController@show');
        $router->put('nodes/{id}', 'V1\NodeController@update');
        $router->delete('nodes/{id}', 'V1\NodeController@destroy');

        $router->get('nodehistories', 'V1\NodehistoryController@index');
        $router->get('nodehistories/{id}', 'V1\NodehistoryController@show');

    });
});





